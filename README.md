<br />
<div align="center">
  <a href="https://crates.io/crates/stk8ba58">
    <img src="images/chip.svg" alt="chip" width="80" height="80">
  </a>

  <h3 align="center">STK8BA58</h3>

  <p align="center">
    Driver crate for the STK8BA58 three axis MEMS accelerometer
    <br />
    <a href="https://docs.rs/stk8ba58"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://gitlab.com/slusheea/stk8ba58/-/issues">Report Bug</a>
    ·
    <a href="https://gitlab.com/slusheea/stk8ba58/-/issues">Request Feature</a>
    <br />
    <br />
  </p>
</div>

![Crates.io Total Downloads](https://img.shields.io/crates/d/stk8ba58?style=for-the-badge&labelColor=%23282a33)
![Crates.io Version](https://img.shields.io/crates/v/stk8ba58?style=for-the-badge&labelColor=%23282a33)
![embedded-hal version](https://img.shields.io/badge/Embedded_HAL-1.0.0-blue?style=for-the-badge&logo=rust&labelColor=%23282a33&color=blue)
![License](https://img.shields.io/badge/License-MIT_or_Apache-blue?style=for-the-badge&labelColor=%23282a33&color=blue)

## Built with
[`embedded_hal`](https://crates.io/crates/embedded-hal) for [`accelerometer.rs`](https://crates.io/crates/accelerometer)

## Getting started
Check out the [documentation](https://docs.rs/stk8ba58) and [examples](/examples) folder for directions on how to use the crate.

## License
The contents of this repository are dual-licensed under the MIT OR Apache 2.0 License. That means you can choose either the MIT license or the Apache-2.0 license when you re-use this code. See `LICENSE-MIT` or `LICENSE-APACHE` for more information on each specific license.

Any submissions to this project (e.g. as Pull Requests) must be made available under these terms.

## Contact
Raise an issue: https://gitlab.com/slusheea/stk8ba58/-/issues or find other ways to contact me on my website: https://slushee.dev/
