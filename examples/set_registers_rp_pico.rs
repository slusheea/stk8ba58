#![no_std]
#![no_main]

use bsp::entry;
use bsp::hal::{
    clocks::init_clocks_and_plls, fugit::RateExtU32, gpio::FunctionI2c, i2c::I2C, pac, sio::Sio,
    watchdog::Watchdog,
};
use rp_pico as bsp;

use defmt::info;
use defmt_rtt as _;
use panic_probe as _;

use stk8ba58::{
    registers::{BwSel, IntLatch, Write, INTCFG2, INTEN1},
    Stk8ba58,
};

#[entry]
fn main() -> ! {
    info!("Program start");
    let mut pac = pac::Peripherals::take().unwrap();
    let mut watchdog = Watchdog::new(pac.WATCHDOG);
    let sio = Sio::new(pac.SIO);

    let clocks = init_clocks_and_plls(
        12_000_000u32,
        pac.XOSC,
        pac.CLOCKS,
        pac.PLL_SYS,
        pac.PLL_USB,
        &mut pac.RESETS,
        &mut watchdog,
    )
    .ok()
    .unwrap();

    let pins = bsp::Pins::new(
        pac.IO_BANK0,
        pac.PADS_BANK0,
        sio.gpio_bank0,
        &mut pac.RESETS,
    );

    let i2c = I2C::i2c1(
        pac.I2C1,
        pins.gpio18.into_function::<FunctionI2c>(),
        pins.gpio19.into_function::<FunctionI2c>(),
        400u32.kHz(),
        &mut pac.RESETS,
        &clocks.system_clock,
    );

    let mut accelerometer = Stk8ba58::new(i2c);

    // Set bandwidth to 250Hz and enable latched Z slope interrupt
    accelerometer.set_bandwidth(BwSel::Hz250).unwrap();
    accelerometer.set_flags(INTEN1::SLP_EN_Z).unwrap();
    accelerometer.set_int_latch(IntLatch::TEMP250MS).unwrap();

    loop {
        // ... Check for the interrupt status

        // Clear interrupt status
        accelerometer.set_flags(INTCFG2::INT_RST).unwrap();
    }
}
