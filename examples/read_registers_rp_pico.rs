#![no_std]
#![no_main]

use bsp::entry;
use bsp::hal::{
    clocks::init_clocks_and_plls, fugit::RateExtU32, gpio::FunctionI2c, i2c::I2C, pac, sio::Sio,
    watchdog::Watchdog,
};
use rp_pico as bsp;

use defmt::info;
use defmt_rtt as _;
use panic_probe as _;

use stk8ba58::{
    registers::{Read, SleepDuration, POWMODE},
    Stk8ba58,
};

#[entry]
fn main() -> ! {
    info!("Program start");
    let mut pac = pac::Peripherals::take().unwrap();
    let mut watchdog = Watchdog::new(pac.WATCHDOG);
    let sio = Sio::new(pac.SIO);

    let clocks = init_clocks_and_plls(
        12_000_000u32,
        pac.XOSC,
        pac.CLOCKS,
        pac.PLL_SYS,
        pac.PLL_USB,
        &mut pac.RESETS,
        &mut watchdog,
    )
    .ok()
    .unwrap();

    let pins = bsp::Pins::new(
        pac.IO_BANK0,
        pac.PADS_BANK0,
        sio.gpio_bank0,
        &mut pac.RESETS,
    );

    let i2c = I2C::i2c1(
        pac.I2C1,
        pins.gpio18.into_function::<FunctionI2c>(),
        pins.gpio19.into_function::<FunctionI2c>(),
        400u32.kHz(),
        &mut pac.RESETS,
        &clocks.system_clock,
    );

    let mut accelerometer = Stk8ba58::new(i2c);

    let mode = accelerometer.read_mode(SleepDuration::default()).unwrap();
    match mode {
        SleepDuration::Ms0C5 => info!("Short nap"),
        SleepDuration::Ms1000 => info!("Deep slumber"),
        _ => info!("Good night sleep"),
    }

    let flags = accelerometer.read_flags(POWMODE::empty()).unwrap();
    if flags.contains(POWMODE::LOWPOWER) {
        info!("accelerometer is in low power mode");
    }

    loop {}
}
