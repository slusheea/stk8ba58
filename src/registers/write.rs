use crate::registers::Write as RegWrite;
use crate::registers::*;
use crate::Stk8ba58;
use core::{fmt::Debug, marker::Copy};

use accelerometer_hal::Error;
use embedded_hal::i2c::I2c;

impl<I2C, E> RegWrite<I2C, E> for Stk8ba58<I2C>
where
    I2C: I2c<Error = E>,
    E: Debug,
{
    /// Sets the sensitivity range of the accelerometer to the [`Register::RANGESEL`]
    ///
    /// Usage:
    /// ```no_run
    /// accelerometer.set_range(RangeSel::PM2G).unwrap();
    /// ```
    ///
    /// # Errors
    ///  - Any I²C errors will cause the function to fail.
    fn set_range(&mut self, range: RangeSel) -> Result<&mut Self, Error<E>> {
        self.write_register(Register::RANGESEL, range as u8)?;
        self.range = range;
        Ok(self)
    }

    /// Sets the bandwidth of the filtered data to the [`Register::BWSEL`] register
    ///
    /// Usage:
    /// ```no_run
    /// accelerometer.set_bandwidth(BwSel::Hz500).unwrap();
    /// ```
    ///
    /// # Errors
    ///  - Any I²C errors will cause the function to fail.
    fn set_bandwidth(&mut self, bandwidth: BwSel) -> Result<&mut Self, Error<E>> {
        self.write_register(Register::BWSEL, bandwidth as u8)?;
        Ok(self)
    }

    /// Sets the sleep time duration to the [`Register::POWMODE`] register
    ///
    /// Usage:
    /// ```no_run
    /// accelerometer.set_sleeptime(SleepTime::Ms10).unwrap();
    /// ```
    ///
    /// # Errors
    ///  - Any I²C errors will cause the function to fail.
    fn set_sleep_duration(&mut self, sleep_duration: SleepDuration) -> Result<&mut Self, Error<E>> {
        let mut buf = [0u8];
        self.read_register(Register::POWMODE, &mut buf)?;

        let mut powmode = buf[0] & 0b111_0000_1; // Keep all the bits except the sleep duration intact
        powmode |= sleep_duration as u8; // Add the sleep time to what already was on the register

        self.write_register(Register::POWMODE, powmode)?;
        Ok(self)
    }

    /// Sets the interrupt latch mode to the [`Register::INTCFG2`] register
    ///
    /// Usage:
    /// ```no_run
    /// accelerometer.set_int_latch(IntLatch::TEMP500US);
    /// ```
    ///
    /// # Errors
    ///  - Any I²C errors will cause the function to fail.
    fn set_int_latch(&mut self, latch: IntLatch) -> Result<&mut Self, Error<E>> {
        let mut buf = [0u8];
        self.read_register(Register::INTCFG2, &mut buf)?;

        let mut int_latch = buf[0] & 0b1111_0000; // Keep the bits we won't change
        int_latch |= latch as u8;

        self.write_register(Register::INTCFG2, int_latch)?;
        Ok(self)
    }

    /// Sets the `flags` to the respective register
    ///
    /// Usage:
    /// ```no_run
    /// let flags = INTEN1::SLP_EN_X | INTEN1::SLP_EN_Y;
    /// accelerometer.set_flags(flags).unwrap();
    /// println!("Enabled slope interrupt for X and Y axis.");
    ///
    /// let flags = INTCFG2::INT_RST;
    /// accelerometer.set_flags(flags).unwrap();
    /// println!("Cleared all latched interrupt pins");
    /// ```
    ///
    /// # Errors
    ///  - Any I²C errors will cause the function to fail.
    fn set_flags<F: Flags<Bits = u8> + Into<Register> + Copy>(
        &mut self,
        flags: F,
    ) -> Result<&mut Self, Error<E>> {
        let mut buf = [0u8];
        self.read_register(flags.into(), &mut buf)?;

        let mut register = buf[0] & F::complement(F::all()).bits(); // Keep what's NOT the flags
        register |= flags.bits(); // Add the new flags to what was on the register already

        self.write_register(flags.into(), register)?;
        Ok(self)
    }

    /// Resets all registers to their default value
    ///
    /// Usage:
    /// ```no_run
    /// accelerometer.reset_all().unwrap();
    /// ```
    ///
    /// # Errors
    ///  - Any I²C errors will cause the function to fail.
    fn reset_all(&mut self) -> Result<&mut Self, Error<E>> {
        // 0xB6 is the magic value that must be written to the `SWRST` register to reset all registers to their default value
        self.write_register(Register::SWRST, 0xB6)?;
        Ok(self)
    }
}
