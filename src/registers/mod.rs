//! STK8BA58 register addresses
#![allow(non_camel_case_types, clippy::unreadable_literal)]

use accelerometer_hal::Error;
use bitflags::{bitflags, Flags};
use core::fmt::Debug;
mod read;
mod write;

/// Register addresses
/// Taken from the STK8BA58 data sheet (Register Map, p.13)
/// <https://datasheet.lcsc.com/lcsc/2101081835_SENSORTEK-STK8BA58_C966931.pdf>
#[derive(Copy, Clone, Debug, Eq, PartialEq)]
#[repr(u8)]
pub enum Register {
    /// Contains the chip identification code.
    CHIP_ID = 0x00,

    /// XOUT1 contains the 4 LSBs of the X-axis acceleration data and the new data
    /// flag for the X-axis
    XOUT1 = 0x02,

    /// XOUT2 contains the 8 MSBs of the X-axis acceleration data and the new data
    /// flag for the X-axis
    XOUT2 = 0x03,

    /// YOUT1 contains the 4 LSBs of the Y-axis acceleration data and the new data
    /// flag for the Y-axis
    YOUT1 = 0x04,

    /// YOUT2 contains the 8 MSBs of the Y-axis acceleration data and the new data
    /// flag for the Y-axis
    YOUT2 = 0x05,

    /// ZOUT1 contains the 4 LSBs of the Z-axis acceleration data and the new data
    /// flag for the Z-axis
    ZOUT1 = 0x06,

    /// ZOUT2 contains the 8 MSBs of the Z-axis acceleration data and the new data
    /// flag for the Z-axis
    ZOUT2 = 0x07,

    /// Contains the interrupts' status
    INTSTS1 = 0x09,

    /// Contains the new data interrupt status
    INTSTS2 = 0x0A,

    /// Contains slope detection information
    EVENTINFO1 = 0x0B,

    /// Contains the acceleration sensing range
    RANGESEL = 0x0F,

    /// Contains the output bandwidth selection
    BWSEL = 0x10,

    /// Contains the power mode selection and the sleep time duration setting
    POWMODE = 0x11,

    /// Used to select if the output data is filtered or unfiltered
    /// and how the output data contained in the register XOUT1/XOUT2,
    /// YOUT1/YOUT2, ZOUT1/ZOUT2 is updated
    DATASETUP = 0x13,

    // Used to software reset. Write `0xB6` (`SWRST`) to reset all registers
    // to the default value
    SWRST = 0x14,

    /// Contans the X, Y, Z slope interrupt enable bits
    INTEN1 = 0x16,

    /// Contains the new data interrupt bit
    INTEN2 = 0x17,

    /// Used to map the related interrupt to the desired INT pin
    INTMAP1 = 0x19,

    /// Used to map the related interrupt to the desired INT pin
    INTMAP2 = 0x1A,

    /// Used to the fine the INT1 pin output type and active level
    INTCFG1 = 0x20,

    /// Used to reset the latched interrupt pin and select the interrupt mode
    INTCFG2 = 0x21,

    /// Used to set the number of samples needed in slope detection
    SLOPEDLY = 0x27,

    /// Used to set the threshold value for slope detection
    SLOPETHD = 0x28,

    /// Used to set the skip time for significant motion
    SIGMOT1 = 0x29,

    /// Contains the MSB of the SKIP_TIME for the significant motion and the
    /// significant motion interrupt enable bit
    SIGMOT2 = 0x2A,

    /// Used to set the proof time for the significant motion
    SIGMOT3 = 0x2B,

    /// Contains the digital interface parameters for the I2C interface
    INTFCFG = 0x34,

    /// Used to define the setting for the offset compensation
    OFSTCOMP1 = 0x36,

    /// Contains the offset compensation for the X-axis
    OFSTX = 0x38,

    /// Contains the offset compensation for the Y-axis
    OFSTY = 0x39,

    /// Contains the ofsset compensation for the Z-axis
    OFSTZ = 0x3A,
}

impl Register {
    /// Get register address
    pub const fn addr(self) -> u8 {
        self as u8
    }

    /// Returns if the register is read only or not
    pub const fn read_only(self) -> bool {
        matches!(
            self,
            Self::CHIP_ID
                | Self::XOUT1
                | Self::XOUT2
                | Self::YOUT1
                | Self::YOUT2
                | Self::ZOUT1
                | Self::ZOUT2
                | Self::INTSTS1
                | Self::INTSTS2
                | Self::EVENTINFO1,
        )
    }
}

// Maybe this could be done with a derive?
/// Expands to implement From<FLAGS_STRUCT> for Register so `.read_flags()` can be called for the
/// type of flag passed
macro_rules! reg_from_flags {
    ($f:ident) => {
        impl From<$f> for Register {
            fn from(_value: $f) -> Self {
                Self::$f
            }
        }
    };
}

reg_from_flags!(INTSTS1);
reg_from_flags!(INTSTS2);
reg_from_flags!(EVENTINFO1);
reg_from_flags!(POWMODE);
reg_from_flags!(DATASETUP);
reg_from_flags!(INTEN1);
reg_from_flags!(INTEN2);
reg_from_flags!(INTMAP1);
reg_from_flags!(INTMAP2);
reg_from_flags!(INTCFG1);
reg_from_flags!(INTCFG2);
reg_from_flags!(SIGMOT2);
reg_from_flags!(INTFCFG);
reg_from_flags!(OFSTCOMP1);

/// `X`, `Y` and `Z` axis. Used for `.axis_lsb()`, `.axis_msb()` and `.axis_newdata()`
pub enum Axis {
    X,
    Y,
    Z,
}

bitflags! {
    /// [`Register::INTSTS1`] options
    #[derive(Copy, Clone)]
    pub struct INTSTS1: u8 {
        /// Significant motion interrupt status
        const SIG_MOT_STS = 0b1;

        /// Any-motion (slope) detection interrupt status
        const ANY_MOT_STS = 0b100;
    }

    /// [`Register::INTSTS2`] options
    #[derive(Copy, Clone)]
    pub struct INTSTS2: u8 {
        /// New data interrupt status
        const DATA_STS = 0b1000_0000;
    }

    /// [`Register::EVENTINFO1`] options
    #[derive(Copy, Clone)]
    pub struct EVENTINFO1: u8 {
        /// Slope interrupt triggered on the X axis
        const SLP_1ST_X = 0b1;

        /// Slope interrupt triggered on the Y axis
        const SLP_1ST_Y = 0b10;

        /// Slope interrupt triggered on the Z axis
        const SLP_1ST_Z = 0b100;

        /// Acceleration on the X axis was negative
        const SLPSIGN_X = 0b1_0000;

        /// Acceleration on the Y axis was negative
        const SLPSIGN_Y = 0b10_0000;

        /// Acceleration on the Z axis was negative
        const SLPSIGN_Z = 0b100_0000;
    }
}

/// Acceleration sensing ranges [`Register::RANGESEL`] supports
#[derive(Copy, Clone, Debug, PartialEq, Eq, Default)]
#[repr(u8)]
pub enum RangeSel {
    /// ±2g
    #[default]
    PM2G = 0b0011,

    /// ±4g
    PM4G = 0b0101,

    /// ±8g
    PM8G = 0b1000,

    /// Undefined range
    Undefined = 0b0,
}

impl From<u8> for RangeSel {
    fn from(value: u8) -> Self {
        // Only last 4 bits are relevant
        match value & 0b1111 {
            0b0011 => Self::PM2G,
            0b0101 => Self::PM4G,
            0b1000 => Self::PM8G,
            _ => Self::Undefined,
        }
    }
}

impl From<RangeSel> for Register {
    fn from(_value: RangeSel) -> Self {
        Self::RANGESEL
    }
}

impl From<RangeSel> for f32 {
    fn from(value: RangeSel) -> Self {
        match value {
            RangeSel::PM2G => 2.0,
            RangeSel::PM4G => 4.0,
            RangeSel::PM8G => 8.0,
            RangeSel::Undefined => 0.0,
        }
    }
}

/// Bandwidths [`Register::BWSEL`] supports
#[derive(Copy, Clone, Debug, PartialEq, Eq, Default)]
#[repr(u8)]
pub enum BwSel {
    /// 7.81Hz
    Hz7C81 = 0b1000,

    /// 15.63Hz
    Hz15C64 = 0b1001,

    /// 31.25Hz
    Hz31C25 = 0b1010,

    /// 62.5Hz
    Hz62C5 = 0b1011,

    /// 125Hz
    Hz125 = 0b1100,

    /// 250Hz
    Hz250 = 0b1101,

    /// 500Hz
    Hz500 = 0b1110,

    /// 1000Hz
    #[default]
    Hz1000 = 0b1111,
}

impl From<u8> for BwSel {
    fn from(value: u8) -> Self {
        // The 3 MSBs are reserved.
        match value & 0b00011111 {
            0..=0b1000 => Self::Hz7C81, // Anything smaller than 0b1001 is equal to 7.81Hz
            0b1001 => Self::Hz15C64,
            0b1010 => Self::Hz31C25,
            0b1011 => Self::Hz62C5,
            0b1100 => Self::Hz125,
            0b1101 => Self::Hz250,
            0b1110 => Self::Hz500,
            _ => Self::Hz1000, // Anything larger than 0b1110 is equal to 1000Hz
        }
    }
}

impl From<BwSel> for f32 {
    fn from(value: BwSel) -> Self {
        match value {
            BwSel::Hz7C81 => 7.81,
            BwSel::Hz15C64 => 15.64,
            BwSel::Hz31C25 => 31.25,
            BwSel::Hz62C5 => 62.5,
            BwSel::Hz125 => 125.0,
            BwSel::Hz250 => 250.0,
            BwSel::Hz500 => 500.0,
            BwSel::Hz1000 => 1000.0,
        }
    }
}

impl From<BwSel> for Register {
    fn from(_value: BwSel) -> Self {
        Self::BWSEL
    }
}

/// Sleep durations [`Register::POWMODE`] supports
#[derive(Copy, Clone, Debug, PartialEq, Eq, Default)]
#[repr(u8)]
pub enum SleepDuration {
    /// 0.5ms
    #[default]
    Ms0C5 = 0b0101_0,

    /// 1ms
    Ms1 = 0b0110_0,

    /// 2ms
    Ms2 = 0b0111_0,

    /// 4ms
    Ms4 = 0b1000_0,

    /// 6ms
    Ms6 = 0b1001_0,

    /// 10ms
    Ms10 = 0b1010_0,

    /// 25ms
    Ms25 = 0b1011_0,

    /// 50ms
    Ms50 = 0b1100_0,

    /// 100ms
    Ms100 = 0b1101_0,

    /// 500ms
    Ms500 = 0b1110_0,

    /// 1000ms
    Ms1000 = 0b1111_0,
}

impl From<u8> for SleepDuration {
    fn from(value: u8) -> Self {
        // Only bits 4-1 are relevant
        match value & 0b1111_0 {
            0b0110_0 => Self::Ms1,
            0b0111_0 => Self::Ms2,
            0b1000_0 => Self::Ms4,
            0b1001_0 => Self::Ms6,
            0b1010_0 => Self::Ms10,
            0b1011_0 => Self::Ms25,
            0b1100_0 => Self::Ms50,
            0b1101_0 => Self::Ms100,
            0b1110_0 => Self::Ms500,
            0b1111_0 => Self::Ms1000,
            _ => Self::Ms0C5, // Anything smaller than 0b0110_0 is 0.5ms
        }
    }
}

impl From<SleepDuration> for Register {
    fn from(_value: SleepDuration) -> Self {
        Self::POWMODE
    }
}

bitflags! {
    /// [`Register::POWMODE`] options
    #[derive(Copy, Clone)]
    pub struct POWMODE: u8 {
        /// Enable low-power mode (Default: Disabled)
        const LOWPOWER = 0b01000000;

        /// Enable suspend mode (Default: Disabled)
        const SUSPEND = 0b10000000;
    }

    /// [`Register::DATASETUP`] options
    #[derive(Copy, Clone)]
    pub struct DATASETUP: u8 {
        /// Disable data protection function (Default: Enabled)
        const PROTECT_DIS = 0b01000000;

        /// Disable data filtering (Default: Enabled)
        const DATA_SEL = 0b10000000;
    }

    /// [`Register::INTEN1`] options
    #[derive(Copy, Clone)]
    pub struct INTEN1: u8 {
        /// Enable X-axis any-motion (slope) interrupt
        /// (Default: Disabled)
        const SLP_EN_X = 0b00000001;

        /// Enable Y-axis any-motion (slope) interrupt
        /// (Default: Disabled)
        const SLP_EN_Y = 0b00000010;

        /// Enable Z-axis any-motion (slope) interrupt
        /// (Default: Disabled)
        const SLP_EN_Z = 0b00000100;
    }

    /// [`Register::INTEN2`] options
    #[derive(Copy, Clone)]
    pub struct INTEN2: u8 {
        /// Enable new data interrupt
        /// (Default: Disabled)
        const DATA_EN = 0b00001000;
    }

    /// [`Register::INTMAP1`] options
    #[derive(Copy, Clone)]
    pub struct INTMAP1: u8 {
        /// Map significant motion interrupt to INT1
        /// (Default: Disabled)
        const SIGMOT2INT1 = 0b00000001;

        /// Map any-motion (slope) interrupt to INT1
        /// (Default: Disabled)
        const ANYMOT2INT1 = 0b0000100;
    }

    /// [`Register::INTMAP2`] options
    #[derive(Copy, Clone)]
    pub struct INTMAP2: u8 {
        /// Map new data interrput to INT1
        /// (Default: Disabled)
        const DATA2INT1 = 0b00000001;
    }

    /// [`Register::INTCFG1`] options
    #[derive(Copy, Clone)]
    pub struct INTCFG1: u8 {
        /// Set INT1 to active high (Default: Enabled)
        const INT1_LV = 0b00000001;

        /// Set INT1 to open drain output (Default: Disabled, Push-pull output)
        const INT1_OD = 0b00000010;
    }
}

/// Interrupt latch modes [`Register::INTCFG2`] supports
#[derive(Copy, Clone, Debug, PartialEq, Eq, Default)]
#[repr(u8)]
pub enum IntLatch {
    #[default]
    NON_LATCHED = 0b0000,
    LATCHED = 0b0111,

    /// Temporary 250us interrupt signal
    TEMP250US = 0b1001,

    /// Temporary 500us interrupt signal
    TEMP500US = 0b1010,

    /// Temporary 1ms interrupt signal
    TEMP1MS = 0b1011,

    /// Temporary 12.5ms interrupt signal
    TEMP12C5MS = 0b1100,

    /// Temporary 25ms interrupt signal
    TEMP25MS = 0b1101,

    /// Temporary 50ms interrupt signal
    TEMP50MS = 0b1110,

    /// Temporary 250ms interrupt signal
    TEMP250MS = 0b0001,

    /// Temporary 500ms interrupt signal
    TEMP500MS = 0b0010,

    /// Temporary 1s interrupt signal
    TEMP1S = 0b0011,

    /// Temporary 2s interrupt signal
    TEMP2S = 0b0100,

    /// Temporary 4s interrupt signal
    TEMP4S = 0b0101,

    /// Temporary 8s interrupt signal
    TEMP8S = 0b0110,
}

impl From<u8> for IntLatch {
    fn from(value: u8) -> Self {
        match value & 0b1111 {
            0b0111 | 0b1111 => Self::LATCHED,
            0b0001 => Self::TEMP250MS,
            0b0010 => Self::TEMP500MS,
            0b0011 => Self::TEMP1S,
            0b0100 => Self::TEMP2S,
            0b0101 => Self::TEMP4S,
            0b0110 => Self::TEMP8S,
            0b1001 => Self::TEMP250US,
            0b1010 => Self::TEMP500US,
            0b1011 => Self::TEMP1MS,
            0b1100 => Self::TEMP12C5MS,
            0b1101 => Self::TEMP25MS,
            0b1110 => Self::TEMP50MS,
            _ => Self::NON_LATCHED,
        }
    }
}

impl From<IntLatch> for Register {
    fn from(_value: IntLatch) -> Self {
        Self::INTCFG2
    }
}

bitflags! {
    /// Reset any latched interrupt pins
    #[derive(Copy, Clone)]
    pub struct INTCFG2: u8 {
        const INT_RST = 0b10000000;
    }

    /// [`Register::SIGMOT2`] options
    #[derive(Copy, Clone)]
    pub struct SIGMOT2: u8 {
        /// Enable significant motion (Default: Enabled)
        const SIG_MOT_EN = 0b00000010;

        /// Enable any-motion (Default: Disabled)
        const ANY_MOT_EN = 0b00000100;
    }

    /// [`Register::INTFCFG`] options
    #[derive(Copy, Clone)]
    pub struct INTFCFG: u8 {
        /// Set watchdog timer to 50ms (Default: 1ms)
        const I2C_WDT_SEL = 0b00000010;

        /// Enable I2C watchdog timer (Default: Disabled)
        const I2C_WDT_EN = 0b00000100;
    }

    /// Reset offset compensation registers
    #[derive(Copy, Clone)]
    pub struct OFSTCOMP1: u8 {
        const OFST_RST = 0b10000000;
    }
}

/// Import this trait if you want to read from the accelerometer's registers
pub trait Read<I2C, E> {
    fn chip_id(&mut self) -> Result<u8, E>;
    fn axis_lsb(&mut self, axis: &Axis) -> Result<u8, E>;
    fn axis_msb(&mut self, axis: &Axis) -> Result<u8, E>;
    fn axis_newdata(&mut self, axis: &Axis) -> Result<bool, E>;

    fn read_flags<F: Flags<Bits = u8> + Into<Register>>(&mut self, flags: F) -> Result<F, E>;
    fn read_mode<M: From<u8> + Into<Register>>(&mut self, mode: M) -> Result<M, E>;
}

/// Import this trait if you want to write to the accelerometer's registers
pub trait Write<I2C, E>
where
    E: Debug,
{
    fn set_range(&mut self, range: RangeSel) -> Result<&mut Self, Error<E>>;
    fn set_bandwidth(&mut self, bandwidth: BwSel) -> Result<&mut Self, Error<E>>;
    fn set_sleep_duration(&mut self, sleep_duration: SleepDuration) -> Result<&mut Self, Error<E>>;
    fn set_int_latch(&mut self, latch: IntLatch) -> Result<&mut Self, Error<E>>;

    fn set_flags<F: Flags<Bits = u8> + Into<Register> + Copy>(
        &mut self,
        flags: F,
    ) -> Result<&mut Self, Error<E>>;

    fn reset_all(&mut self) -> Result<&mut Self, Error<E>>;
}
