use crate::registers::{Axis, Read, Register};
use crate::Stk8ba58;
use bitflags::Flags;
use core::fmt::Debug;
use embedded_hal::i2c::I2c;

impl<I2C, E> Read<I2C, E> for Stk8ba58<I2C>
where
    I2C: I2c<Error = E>,
    E: Debug,
{
    /// Reads the chip ID from the [`Register::CHIP_ID`] register
    ///
    /// # Errors
    ///  - Any I²C errors will cause the function to fail.
    fn chip_id(&mut self) -> Result<u8, E> {
        let mut buf = [0u8];
        self.read_register(Register::CHIP_ID, &mut buf)?;
        Ok(buf[0])
    }

    /// Reads the axis' acceleration value's LSBs from the respective register
    ///
    /// **Note**: If you want to read the acceleration or raw acceleration values, use the
    /// `Accelerometer` or  `RawAccelerometer` traits. If you're sure you want to use this,
    /// remember the LSBs should be read first if data protection is enabled.
    ///
    /// # Errors
    ///  - Any I²C errors will cause the function to fail.
    fn axis_lsb(&mut self, axis: &Axis) -> Result<u8, E> {
        let mut buf = [0];
        let register = match axis {
            Axis::X => Register::XOUT1,
            Axis::Y => Register::YOUT1,
            Axis::Z => Register::ZOUT1,
        };
        self.read_register(register, &mut buf)?;
        Ok(buf[0] & 0b1111_0000) // Only the first 4 bits are relevant
    }

    /// Reads the axis' acceleration value's MSBs from the respective register
    ///
    /// **Note**: If you want to read the acceleration or raw acceleration values, use the
    /// `Accelerometer` or  `RawAccelerometer` traits. If you're sure you want to use this,
    /// remember the LSBs should be read first if data protection is enabled.
    ///
    /// # Errors
    ///  - Any I²C errors will cause the function to fail.
    fn axis_msb(&mut self, axis: &Axis) -> Result<u8, E> {
        let mut buf = [0];
        let register = match axis {
            Axis::X => Register::XOUT2,
            Axis::Y => Register::YOUT2,
            Axis::Z => Register::ZOUT2,
        };
        self.read_register(register, &mut buf)?;
        Ok(buf[0])
    }

    /// Reads the axis' new data flag from the respective register
    ///
    /// **Note**: The flag will be reset after the register is read.
    fn axis_newdata(&mut self, axis: &Axis) -> Result<bool, E> {
        let mut buf = [0];
        let register = match axis {
            Axis::X => Register::XOUT1,
            Axis::Y => Register::YOUT1,
            Axis::Z => Register::ZOUT1,
        };
        self.read_register(register, &mut buf)?;
        Ok((buf[0] & 0b1) == 0b1)
    }

    /// Read the mode set to the respective register.
    ///
    /// Usage:
    /// ```no_run
    /// let mode = accelerometer.read_mode(SleepDuration::default()).unwrap();
    /// match mode {
    ///     SleepDuration::Ms0C5 => info!("Short nap"),
    ///     SleepDuration::Ms1000 => info!("Deep slumber"),
    ///     _ => info!("Good night sleep"),
    /// }
    /// ```
    ///
    /// # Errors
    ///  - Any I²C errors will cause the function to fail.
    fn read_mode<M: From<u8> + Into<Register>>(&mut self, mode: M) -> Result<M, E> {
        let mut buf = [0u8];
        self.read_register(mode.into(), &mut buf)?;
        Ok(buf[0].into())
    }

    /// Read the `flags` from the respective register
    ///
    /// Usage:
    /// ```no_run
    /// let flags = accelerometer.read_flags(POWMODE::empty()).unwrap();
    /// if flags.contains(POWMODE::SUSPEND) {
    ///     println!("Accelerometer is sleepy...");
    /// }
    /// ```
    ///
    /// # Errors
    ///  - Any I²C errors will cause the function to fail.
    fn read_flags<F: Flags<Bits = u8> + Into<Register>>(&mut self, flags: F) -> Result<F, E> {
        let mut buf = [0u8];
        self.read_register(flags.into(), &mut buf)?;
        Ok(F::from_bits_truncate(buf[0]))
    }
}
